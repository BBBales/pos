#include "OrderForm.h"
#include "TableWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
      OrderForm order;
//    order.show();
//    order.hide();
      TableWindow tables;
/*
 * Currently the TableWindow is activated upon use in the constructor.
 * TODO: Activate TableWindow from another gui window with a proper connection
 */
//      tables.show();
//    MainWindow window;
//    window.show();

      //GENERIC CONNECTION TEMPLATE
    //QObject::connect(&CALLING_OBJECT,SIGNAL(CALLING_FUNCTION(ARGS)), &RECIEVING_OBJECT, SLOT(RECIEVING_FUNCTION(ARGS)));
      QObject::connect(&tables, SIGNAL(tableSelect(int, int)),&order, SLOT(setTable(int, int)));


    return a.exec();
}
