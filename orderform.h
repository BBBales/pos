#ifndef ORDERFORM_H
#define ORDERFORM_H

#include <QWidget>
#include "OrderTicket.h"

namespace Ui {
class OrderForm;
}

class OrderForm : public QWidget
{
    Q_OBJECT

public:
    explicit OrderForm(QWidget *parent = 0);
    ~OrderForm();


private:
    const double TAX = .06;
    OrderTicket ticket;
    int getEmpNum();
    void setEmpNum(int emp);
    int getTableNum();
    void setTableNum(int table);
    void showOrderPage();
    void calculateTotals(double subtotal);


private slots:

    void on_AddItemButton_clicked();
    void on_RemoveItemButton_clicked();
    void on_CashOutButton_clicked();
    void on_CancelOrderButton_clicked();
    void on_SaveOrderButton_clicked();
    void on_custSelectSpin_valueChanged(int custNum);

public slots:
    void setTable(int table, int emp);

private:
    Ui::OrderForm *ui;
};

#endif // ORDERFORM_H
